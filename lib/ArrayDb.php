<?php

/**
 * Classe qui stocke le contenu d'une matrice dans une base de données Sqlite
 */
class ArrayDb
{
  /**
   *
   * @var SQLite3 
   */
  private $cn;
  
  /**
   *
   * @var int 
   */
  private $dimension;
  
  /**
   * 
   * @param string $filename
   */
  public function __construct(string $filename, int $dimension, $rewrite = false)
  {
    $exists = file_exists($filename);
    $this->dimension = $dimension;
    $this->cn = new SQLite3($filename);
    // if (!$exists) {
    $this->cn->exec("PRAGMA journal_mode=MEMORY");
    if ($rewrite) {
      $this->prepare_tables();
    }
    $this->cn->exec('BEGIN');
    // }
  }
  
  public function getcn(): SQLite3
  {
    return $this->cn;
  }
  
  public function flush()
  {
    $this->cn->exec('COMMIT');
  }
  
  public function __destruct()
  {
    $this->flush();
  }
  
  protected function prepare_tables()
  {
    $keys = [];
    $pk = [];
    for ($idx = 0; $idx < $this->dimension; ++$idx) {
      $keys[] = "key" . $idx . " integer";
      $pk[] = "key" . $idx;
    }
    $this->cn->exec("DROP TABLE IF EXISTS matrix");
    $sql = "CREATE TABLE matrix (" . 
        implode(", ", $keys) .
        ", value integer" .
        ", PRIMARY KEY (" . implode(', ', $pk) .
        ") )";
    $this->cn->exec($sql);
 }
  
  /**
   * 
   * @param array $index
   * @return int
   */
  public function get(... $index): int
  {
    return $this->cn->querySingle("SELECT value FROM matrix WHERE " . $this->getSqlIndex($index)); 
  }
  
  /**
   * 
   * @param string $what
   * @param string $where
   * @param string $groupby
   * @param string $orderby
   * @return Sqlite3Result
   */
  public function query($what, $where = null, $groupby = null, $orderby = null): SQLite3Result
  {
    $sql = "SELECT $what FROM matrix";
    if (!is_null($where)) {
      $sql .= " WHERE $where";
    }
    if (!is_null($groupby)) {
      $sql .= " GROUP BY $groupby";
    }
    if (!is_null($orderby)) {
      $sql .= " ORDER BY $orderby";
    }
    // var_dump($sql);
    return $this->cn->query($sql);
  }
  
  /**
   * 
   * @param int $value
   * @param array $index
   */
  public function put($value, ... $index)
  {
    $whereIndex = $this->getSqlIndex($index);
    $ct = $this->cn->querySingle("SELECT count(*) FROM matrix WHERE $whereIndex");
    if ($ct == 0) {
      $this->insert($value, $index);
    } else {
      $this->cn->exec("UPDATE matrix SET value = $value WHERE $whereIndex");    
    }
  }
  
  protected function insert($value, array $index)
  {
    $fields = "";
    $values = "";
    for ($idx = 0; $idx < $this->dimension; ++$idx) {
      $fields .= ", key" . (string)($idx);
      $values .= ", " . $index[$idx];
    }
    $this->cn->exec("INSERT INTO matrix (value $fields) VALUES ($value $values)");
  }
  
  public function inc(... $index) 
  {
    $whereIndex = $this->getSqlIndex($index);
    $ct = $this->cn->querySingle("SELECT count(*) FROM matrix WHERE $whereIndex");
    if ($ct == 0) {
      // S'il n'existe pas on considère la valeur comme nulle
     $this->insert(1, $index);
    } else {
      $this->cn->exec("UPDATE matrix SET value=value + 1 WHERE " . $whereIndex);
    }
  }
  
  private function getSqlIndex(array $index): string
  {
    $sql = [];
    for ($idx = 0; $idx < $this->dimension; ++$idx)
    {
      $sql[] = "key" . (string)($idx) . ' = ' . $index[$idx];
    }
    return implode(" AND ", $sql);
  }
}
