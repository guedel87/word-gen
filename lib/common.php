<?php
	function path_join($head, $tail) 
	{
		return join(DIRECTORY_SEPARATOR, array($head, $tail));
	}

	function array_zeros($dimensions) 
	{
		if (is_array($dimensions)) {
			if (count($dimensions) > 1) {
				$arr = new \SplFixedArray($dimensions[0]);
				for ($i = 0; $i < $dimensions[0]; ++$i) {
					$arr[$i] = array_zeros(array_slice($dimensions, 1));
				}
				return $arr;
			} else {
				return array_zeros($dimensions[0]);
			}
		} else {
			$arr = new \SplFixedArray($dimensions);
			for ($i = 0; $i < $dimensions; ++$i) {
				$arr[$i] = 0;
			}
			return $arr;
		}
	}

	function save_array($file, SplFixedArray $array)
	{
		fwrite($file, pack('L', $array->count()));
		foreach($array as $value) {
			if ($value instanceof SplFixedArray) {
				save_array($file, $value);
			} else {
				fwrite(serialize($value));
			}
		}
	}

	function retrieve_array($file): SplFixedArray
	{
		$count = unpack('L', fread($file, 4));
		$array = new SplFixedArray($count);
	}

  /**
   * Effectue un tirage aléeatoire dans une liste en fonction des poids
   * @param array $array un tableau avec en valeur les différents poids
   * @return mixed l'index du tableau qui est tiré
   */
  function weighted_choice($array)
  {
    asort($array);
    $total = array_sum($array);
    $alea = random_int(0, $total);
    $seek = 0;
    $resultat = 0;
    foreach($array as $key => $value) {
      if ($alea <= $seek + $value) {
        $resultat = $key;
        break;
      }
      $seek += $value;
    }
    return $resultat;
  }
    