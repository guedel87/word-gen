<?php
	ini_set('memory_limit', -1);
	
	require 'lib/common.php';
    require 'lib/ArrayDb.php';
	
	function process()
	{
      global $lang, $codec, $randomSeed, $max_count, $min_length, $max_length;
      global $verbose;
		if (! $randomSeed) {
			// permet de reproduire toujours la méme série.
			srand(1);
		}
		$filepath = path_join("words", sprintf("%s.txt", $lang));
		$outfile = path_join("outputs", sprintf("%s.txt", $lang));
		$probafile = path_join("counts", sprintf("%s.db", $lang));
		
		try {
			$dico = file($filepath, FILE_SKIP_EMPTY_LINES | FILE_IGNORE_NEW_LINES );
			$count = new ArrayDb($probafile, 3);
            
            // Nb de tirage de mot
            if ($verbose) {
              // $max_count = 1;
              echo "for ct in [0 .. $max_count]", PHP_EOL;
            } 
            for ($ct = 0; $ct < $max_count; ++$ct) {
              // longueur du mot
              $i = 0;
              $j = 0;
              $length = random_int($min_length, $max_length);
              if ($verbose) {
                echo "take word of length $length", PHP_EOL;
              }
              $word = '';
              for ($l = 1; $l <= $length; ++$l) {
                if ($l == $length) {
                  if ($verbose) {
                    echo "$l- find sequence with key0=$i and key1=$j and key2=0", PHP_EOL;
                  }
                  $cnt = $count->getcn()->querySingle("SELECT count(*) FROM matrix WHERE key0=$i and key1=$j and key2=0");
                  if ($cnt == 0) {
                    // on ne trouve pas de terminaison, alors on prolonge le mot.
                    $l--;
                    $length++;
                    continue;
                  }
                  break;
                } else {
                  if ($verbose) {
                    echo "$l- find sequence with key0=$i and key1=$j AND key2!=0", PHP_EOL;
                  }
                  $result = $count->query("key2, value", "key0=$i AND key1=$j AND key2 != 0", "key2", "key2");
                  $detail = [];
                  while ($row = $result->fetchArray(SQLITE3_BOTH) ) {
                    if ($verbose) {
                      // var_dump($row);
                    }
                    $detail[$row[0]] = $row[1];
                  }
                  $result->finalize();
                // Tirage du caractère
                  $k = weighted_choice($detail);
                  if ($verbose) {
                    echo "get ascii code $k", PHP_EOL;
                  }
                  $word .= chr($k);
                }
                
                // On passe au suivant
                $i = $j;
                $j = $k;
              }
              echo $word;
              if (array_search($word, $dico)) {
                echo '*';
              }
              echo PHP_EOL;
            }
		} catch (Exception $e) {
			echo $e->getTraceAsString();
		}
	}
	
	$options = getopt('hv', array('help', 'verbose', 'lang:', 'codec:', 'random-seed', 'no-random-seed', 'count:', 'min:', 'max:'));
	
	if (isset($options['help']) || isset($options['h'])) {
		echo "options:\n";
		echo "\t-h, --help: this help\n";
		echo "\t--lang=<LANG> : language. Default FR\n";
		echo "\t--codec=<CODEC>: charset encoding. Default ISO-8859-1\n";
		echo "\t--random-seed, --no-random-seed\n";
        echo "\t--count=<COUNT>: number of outputs. Default=20\n";
        echo "\t--min=<MIN>: minimal length of words. Default=7\n";
        echo "\t--max=<MAX>: maximal length of words. Default=16\n";
        echo "\t-v, --verbose: verbose mode\n";
	}
	
	$lang = 'FR';
    $verbose = false;
    if (isset($options['verbose']) || isset($options['v']) ) {
      $verbose = true;
    }
	if (isset($options['lang'])) {
		$lang = $options['lang'];
	}
	$codec = 'ISO-8859-1';
	if (isset($options['codec'])) {
		$codec = $options['codec'];
	}
	$randomSeed = true;
	if (isset($options['random-seed'])) {
		$randomSeed = true;
	}
	if (isset($options['no-random-seed'])) {
		$randomSeed = false;
	}
    $max_count = 20;
    if (isset($options['count'])) {
      $max_count = $options['count'];
    }
    $min_length=7;
    $max_length=16;
    if (isset($options['min'])) {
      $min_length = $options['min'];
    }
    if (isset($options['max'])) {
      $max_length = $options['max'];
    }
	
	// Pour vérifier les paramètres
	// print_r(compact(explode(' ', 'lang codec randomSeed max_count min_length max_length')));
	
	if ($randomSeed) {
		echo 'Using a truly random seed';
		echo 'Use --no-random-seed to get reproducible results';
	}
    process($lang, $codec, $randomSeed, $max_count, $min_length, $max_length);
