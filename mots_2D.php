<?php

ini_set('memory_limit', -1);

require 'lib/common.php';
require 'lib/ArrayDb.php';

function lang_count($lang, ArrayDb $count)
{
  $cn = $count->getcn();
  $cn->enableExceptions(true);
  $cn->exec("DROP TABLE IF EXISTS words");
  $cn->exec("CREATE TABLE words (w TEXT PRIMARY KEY)");
  $filepath = path_join("words", sprintf("%s.txt", $lang));

  // $file = new \SplFileObject($filepath);
  $dico = file($filepath, FILE_SKIP_EMPTY_LINES | FILE_IGNORE_NEW_LINES);

  echo "\n";
  //foreach ($dico as $line) {
  $ct = count($dico);
  for ($idx = 0; $idx < $ct; ++$idx) {
    // Affichage de la progression
    echo "\r", round($idx / $ct * 100, 1), ' % ';
    // $l = mb_convert_encoding(str_replace(array("\r", "\n"), '', $file->fgets()), "UTF-8", "ISO-8859-1" );
    // echo "\r", round($file->ftell() / $file->getSize() * 100, 1), ' % ';
    //$l = iconv("ISO-8859-1", "UTF-8", str_replace(array("\r", "\n"), '', $file->fgets()));
    //$l = iconv("ISO-8859-1", "UTF-8", str_replace(array("\r", "\n"), '', $dico[$idx]));
    $l = str_replace(array("\r", "\n"), '', $dico[$idx]);
    $l2 = preg_split('/[\s,\(]+/', $l);
    foreach ($l2 as $mot) {
      // echo $mot, ' ';
      // $mot = strtolower($mot);
      $i = 0;
      $j = 0;
      try {
        $mot2 = $mot;
        $cn->exec("INSERT INTO words VALUES('$mot2')");
      } catch(Exception $e) {
        echo PHP_EOL, $mot2, ": ", $e->getMessage(), PHP_EOL;
      }
      //for ($idc = 0; $idc < strlen($mot); ++$idc) {
      //  $c = substr($mot, $idc, 1);
      foreach (str_split($mot) as $c) {
        try {
          $k = ord($c);
          $count->inc($i, $j, $k);
        } catch(Exception $e) {
          echo PHP_EOL, "$c ($k): ", $e->getMessage(), PHP_EOL;
        }
        $i = $j;
        $j = $k;
      }
      // On ajoute pour la fin de mot
      $count->inc($i, $j, 0);
    }
  }
}

// iconv_set_encoding("input_encoding", "ISO-8859-1");
// ini_set('input_encoding', 'ISO-8859-1');
$options = getopt('', array('lang::'));
$lang = 'FR';
if (isset($options['lang'])) {
  $lang = $options['lang'];
}

// Ecriture dans un fichier binaire
$file = path_join('counts', sprintf("%s.db", $lang));
$count = new ArrayDb($file, 3, true);
lang_count($lang, $count);

